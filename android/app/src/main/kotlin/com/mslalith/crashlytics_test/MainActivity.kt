package com.mslalith.crashlytics_test

import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

const val CRASH_CHANNEL_NAME = "com.crashlytics_test_channel"

class MainActivity : FlutterActivity() {

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CRASH_CHANNEL_NAME).setMethodCallHandler { call, result ->
            if (call.method == "crashFromAndroid") {
                val list = listOf<String>()
                println("list[10]: ${list[10]}")
                return@setMethodCallHandler result.success(true)
            }
        }
    }
}
