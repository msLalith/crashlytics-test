// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../pages/date_of_birth_form_page.dart';
import '../pages/home_page.dart';
import '../pages/page_with_data.dart';
import '../pages/simple_page.dart';

class Routes {
  static const String homePage = '/';
  static const String dateOfBirthFormPage = '/date-of-birth-form-page';
  static const String simplePage = '/simple-page';
  static const String pageWithData = '/page-with-data';
  static const all = <String>{
    homePage,
    dateOfBirthFormPage,
    simplePage,
    pageWithData,
  };
}

class NavigationRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.homePage, page: HomePage),
    RouteDef(Routes.dateOfBirthFormPage, page: DateOfBirthFormPage),
    RouteDef(Routes.simplePage, page: SimplePage),
    RouteDef(Routes.pageWithData, page: PageWithData),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    HomePage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => HomePage(),
        settings: data,
      );
    },
    DateOfBirthFormPage: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => DateOfBirthFormPage(),
        settings: data,
      );
    },
    SimplePage: (data) {
      final args = data.getArgs<SimplePageArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => SimplePage(
          key: args.key,
          string: args.string,
          integer: args.integer,
        ),
        settings: data,
      );
    },
    PageWithData: (data) {
      final args = data.getArgs<PageWithDataArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => PageWithData(
          key: args.key,
          title: args.title,
          intList: args.intList,
          map: args.map,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Navigation helper methods extension
/// *************************************************************************

extension NavigationRouterExtendedNavigatorStateX on ExtendedNavigatorState {
  Future<dynamic> pushHomePage() => push<dynamic>(Routes.homePage);

  Future<dynamic> pushDateOfBirthFormPage() =>
      push<dynamic>(Routes.dateOfBirthFormPage);

  Future<dynamic> pushSimplePage({
    Key key,
    @required String string,
    @required int integer,
  }) =>
      push<dynamic>(
        Routes.simplePage,
        arguments:
            SimplePageArguments(key: key, string: string, integer: integer),
      );

  Future<dynamic> pushPageWithData({
    Key key,
    String title = 'Default Title',
    @required List<int> intList,
    @required Map<String, dynamic> map,
  }) =>
      push<dynamic>(
        Routes.pageWithData,
        arguments: PageWithDataArguments(
            key: key, title: title, intList: intList, map: map),
      );
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// SimplePage arguments holder class
class SimplePageArguments {
  final Key key;
  final String string;
  final int integer;
  SimplePageArguments(
      {this.key, @required this.string, @required this.integer});
}

/// PageWithData arguments holder class
class PageWithDataArguments {
  final Key key;
  final String title;
  final List<int> intList;
  final Map<String, dynamic> map;
  PageWithDataArguments(
      {this.key,
      this.title = 'Default Title',
      @required this.intList,
      @required this.map});
}
