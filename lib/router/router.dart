import 'package:auto_route/auto_route_annotations.dart';
import 'package:crashlytics_test/pages/date_of_birth_form_page.dart';
import 'package:crashlytics_test/pages/home_page.dart';
import 'package:crashlytics_test/pages/page_with_data.dart';
import 'package:crashlytics_test/pages/simple_page.dart';

@MaterialAutoRouter(
  routes: <AutoRoute>[
    MaterialRoute(page: HomePage, initial: true),
    MaterialRoute(page: DateOfBirthFormPage),
    MaterialRoute(page: SimplePage),
    MaterialRoute(page: PageWithData),
  ],
  generateNavigationHelperExtension: true,
)
class $NavigationRouter {

}