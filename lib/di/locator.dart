import 'package:crashlytics_test/services/notification_service.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setupLocator() {
  getIt.registerLazySingleton<NotificationService>(() => NotificationService());
}
