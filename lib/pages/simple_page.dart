import 'package:flutter/material.dart';

class SimplePage extends StatelessWidget {
  final String string;
  final int integer;

  const SimplePage({
    Key key,
    @required this.string,
    @required this.integer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Simple Page'),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('String: $string'),
            const SizedBox(height: 8.0),
            Text('int: $integer'),
          ],
        ),
      ),
    );
  }
}
