import 'package:flutter/material.dart';

class PageWithData extends StatelessWidget {
  final String title;
  final List<int> intList;
  final Map<String, dynamic> map;

  const PageWithData({
    Key key,
    this.title = 'Default Title',
    @required this.intList,
    @required this.map,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page with Data'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('title: $title'),
            const SizedBox(height: 8.0),
            Text('intList: $intList'),
            const SizedBox(height: 8.0),
            Text('map: $map'),
          ],
        ),
      ),
    );
  }
}
