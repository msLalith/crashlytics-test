import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DateOfBirthFormPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Date Of Birth'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 12.0,
          vertical: 12.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 4.0, bottom: 8.0),
              child: Text(
                'Date Of Birth',
                style: Theme.of(context).textTheme.subtitle2,
              ),
            ),
            DateOfBirthTextField(),
          ],
        ),
      ),
    );
  }
}

class DateOfBirthTextField extends StatefulWidget {
  @override
  _DateOfBirthTextFieldState createState() => _DateOfBirthTextFieldState();
}

class _DateOfBirthTextFieldState extends State<DateOfBirthTextField> {
  TextEditingController _dateOfBirthController = TextEditingController();

  @override
  void dispose() {
    _dateOfBirthController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _dateOfBirthController,
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        DateOfBirthTextInputFormatter(),
      ],
      decoration: InputDecoration(
        hintText: 'DD/MM/YYYY',
        border: OutlineInputBorder(),
      ),
    );
  }
}

class DateOfBirthTextInputFormatter extends TextInputFormatter {
  final String mask;
  final String separator;

  DateOfBirthTextInputFormatter({
    this.mask = '##/##/####',
    this.separator = '/',
  });

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    final oldText = oldValue.text;
    final newText = newValue.text;
    final oldValueLength = oldText.length;
    final newValueLength = newText.length;
    final digitsAndSlashRegEx = RegExp(r'[\d\/]');
    if (newValueLength > mask.length) return oldValue;

    if (newText.isEmpty) return newValue;
    if (!digitsAndSlashRegEx.hasMatch(newText)) return oldValue;

    if (!newText.endsWith(separator) && mask[newValueLength - 1] == separator) {
      return _addSeparator(newValue, shouldAddBefore: true);
    }

    if (newValueLength > 0 && newValueLength > oldValueLength) {
      if (newValueLength < mask.length && mask[newValueLength] == separator) {
        return _addSeparator(newValue);
      }
    }
    return newValue;
  }

  TextEditingValue _addSeparator(
    TextEditingValue value, {
    bool shouldAddBefore = false,
  }) {
    String text = value.text;
    int selection = value.selection.end + 1;
    if (shouldAddBefore) {
      final separateIndex = text.length - 1;
      final before = text.substring(0, separateIndex);
      final after = text.substring(separateIndex);
      text = '$before/$after';
      selection += 1;
    } else {
      text = '$text/';
    }
    return TextEditingValue(
      text: text,
      selection: TextSelection.collapsed(offset: selection),
    );
  }
}
