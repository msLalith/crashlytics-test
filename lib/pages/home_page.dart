import 'package:crashlytics_test/crash/crash_channel.dart';
import 'package:crashlytics_test/di/locator.dart';
import 'package:crashlytics_test/services/notification_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Crashlytics Test'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              child: Text('Crash from Android'),
              onPressed: _crashFromAndroid,
            ),
            ElevatedButton(
              child: Text('Crash from iOS'),
              onPressed: _crashFromIOS,
            ),
            ElevatedButton(
              child: Text('Crash from Flutter'),
              onPressed: _crashFromFlutter,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _crashFromAndroid() async {
    await CrashChannel.crashFromAndroid();
  }

  Future<void> _crashFromIOS() async {
    await CrashChannel.crashFromIOS();
  }

  void _crashFromFlutter() {
    final String nullValue = null;
    print(nullValue.isEmpty);
  }
}
