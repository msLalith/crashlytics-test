import 'package:auto_route/auto_route.dart';
import 'package:catcher/catcher.dart';
import 'package:crashlytics_test/crash/crashlytics_handler.dart';
import 'package:crashlytics_test/firebase_cloud_messaging.dart';
import 'package:crashlytics_test/router/router.gr.dart';
import 'package:crashlytics_test/crash/sentry_handler.dart';
import 'package:crashlytics_test/start_up_initializer.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';

Future<void> main() async {
  initCatcherAndRun(
    runApp: () async {
      await startUpInitializer();
      runApp(MyApp());
    },
  );
}

void initCatcherAndRun({void Function() runApp}) {
  final debugOptions = CatcherOptions(
    SilentReportMode(),
    [
      ToastHandler(),
      // CrashlyticsHandler(),
      // SentryReportHandler(),
    ],
  );

  final runAppFunction = () {
    FlutterError.onError = (details) {
      Catcher.reportCheckedError(
        details.exception,
        details.stack,
      );
    };
    runApp();
  };

  Catcher(
    runAppFunction: runAppFunction,
    debugConfig: debugOptions,
    enableLogger: true,
    ensureInitialized: true,
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    FirebaseMessaging.instance.getInitialMessage().then(
      (message) {
        if (message != null) {
          onMessageReceived(
            message,
            appState: FirebaseMessagingAppState.TERMINATED,
          );
          ExtendedNavigator.root.pop();
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return OverlaySupport(
      child: MaterialApp(
        builder: ExtendedNavigator.builder<NavigationRouter>(
          router: NavigationRouter(),
          initialRoute: Routes.homePage,
        ),
      ),
    );
  }
}
