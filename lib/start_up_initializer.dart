import 'package:crashlytics_test/di/locator.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import 'firebase_cloud_messaging.dart';

const _SENTRY_DSN =
    'https://bd2f7beb31a8448085da20283a136ae8@o501308.ingest.sentry.io/5582275';

Future<void> startUpInitializer() async {
  await SentryFlutter.init((options) => options.dsn = _SENTRY_DSN);
  await Firebase.initializeApp();
  setupLocator();
  await configureFirebaseMessaging();
}
