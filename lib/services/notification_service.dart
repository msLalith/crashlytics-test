import 'dart:convert' show jsonDecode;
import 'dart:math' show Random;

import 'package:auto_route/auto_route.dart';
import 'package:crashlytics_test/router/router.gr.dart';
import 'package:crashlytics_test/widgets/pop_up_notification.dart';
import 'package:flutter/material.dart' show Color, required;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:overlay_support/overlay_support.dart';

class NotificationService {
  static const String ANDROID_CHANNEL_ID = '983426598';
  static const String ANDROID_CHANNEL_NAME = 'android_channel_name';
  static const String ANDROID_CHANNEL_DESCRIPTION =
      'android_channel_description';

  final FlutterLocalNotificationsPlugin _localNotification =
      FlutterLocalNotificationsPlugin();

  NotificationService() {
    _initLocalNotification();
  }

  Future<void> _initLocalNotification() async {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings();
    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );
    await _localNotification.initialize(
      initializationSettings,
      onSelectNotification: onSelectNotification,
    );
  }

  void showNotification({
    @required String title,
    @required String body,
    String payload,
    DateTime when,
    Color appIconColor,
  }) {
    final random = Random();
    final id = random.nextInt(100) * random.nextInt(100);
    final androidNotification = AndroidNotificationDetails(
      ANDROID_CHANNEL_ID,
      ANDROID_CHANNEL_NAME,
      ANDROID_CHANNEL_DESCRIPTION,
      importance: Importance.max,
      priority: Priority.high,
      when: when?.millisecondsSinceEpoch,
      color: appIconColor,
      styleInformation: BigTextStyleInformation(body),
    );
    final notificationDetails =
        NotificationDetails(android: androidNotification);
    _localNotification.show(
      id,
      title,
      body,
      notificationDetails,
      payload: payload,
    );
  }

  void showNotificationOverlay({
    @required String title,
    @required String body,
    String payload,
    Duration duration = const Duration(milliseconds: 4000),
  }) {
    showOverlayNotification(
      (context) => PopUpNotification(
        title: title,
        body: body,
        payload: payload,
      ),
      duration: duration,
    );
  }

  Future<dynamic> onSelectNotification(String payload) {
    print("==================== PAYLOAD ======================");
    print(payload);
    print("================== PAYLOAD END ====================");

    if (payload != null && payload != '{}') {
      final data = jsonDecode(payload);
      final routeName = data['routeName'] as String;

      switch (routeName) {
        case Routes.homePage:
          ExtendedNavigator.root.pushHomePage();
          break;
        case Routes.dateOfBirthFormPage:
          ExtendedNavigator.root.pushDateOfBirthFormPage();
          break;
        case Routes.simplePage:
          final string = data['string'] ?? '';
          final integer = int.parse(data['integer']) ?? 0;
          ExtendedNavigator.root.pushSimplePage(
            string: string,
            integer: integer,
          );
          break;
        case Routes.pageWithData:
          final payload = jsonDecode(data['payload'] ?? '{}');
          final title = payload['title'] as String ?? '';
          final intList = List<int>.from(payload['intList'] ?? []);
          final map = Map<String, dynamic>.from(payload['map'] ?? {});
          ExtendedNavigator.root.pushPageWithData(
            title: title,
            intList: intList,
            map: map,
          );
          break;
        default:
          break;
      }
    }
    return null;
  }
}
