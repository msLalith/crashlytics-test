import 'dart:convert' show jsonEncode;

import 'package:crashlytics_test/di/locator.dart';
import 'package:crashlytics_test/services/notification_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

enum FirebaseMessagingAppState {
  RUNNING,
  BACKGROUND,
  TERMINATED,
}

Future<void> configureFirebaseMessaging() async {
  await FirebaseMessaging.instance.getToken();
  // Invoked when app is terminated.
  FirebaseMessaging.onBackgroundMessage(
    (message) => onMessageReceived(
      message,
      appState: FirebaseMessagingAppState.TERMINATED,
    ),
  );

  // Invoked when app is in background state upon Notification click.
  FirebaseMessaging.onMessageOpenedApp.listen(
    (message) => onMessageReceived(
      message,
      appState: FirebaseMessagingAppState.BACKGROUND,
      isFromNotificationClick: true,
    ),
  );

  // Invoked when app is in foreground state.
  FirebaseMessaging.onMessage.listen(onMessageReceived);
}

Future<void> onMessageReceived(
  RemoteMessage message, {
  FirebaseMessagingAppState appState = FirebaseMessagingAppState.RUNNING,
  bool isFromNotificationClick = false,
}) async {
  if (message == null) return;

  print(_remoteMessageToString(message, appState, isFromNotificationClick));

  final notificationService = getIt<NotificationService>();
  if (appState == FirebaseMessagingAppState.RUNNING) {
    notificationService.showNotificationOverlay(
      title: message.notification.title,
      body: message.notification.body,
      payload: jsonEncode(message.data),
    );
  } else {
    notificationService.onSelectNotification(jsonEncode(message.data));
  }
}

String _remoteMessageToString(
  RemoteMessage message,
  FirebaseMessagingAppState appState,
  bool isFromNotificationClick,
) {
  return '''FCM(
  appState: $appState, 
  isFromNotificationClick: $isFromNotificationClick,
  message: Message(
    senderId: ${message.senderId},
    category: ${message.category},
    collapseKey: ${message.collapseKey},
    contentAvailable: ${message.contentAvailable},
    data: ${message.data},
    from: ${message.from},
    messageId: ${message.messageId},
    messageType: ${message.messageType},
    mutableContent: ${message.mutableContent},
    notification: RemoteNotification(
      android: AndroidNotification(
        channelId: ${message.notification.android.channelId},
        clickAction: ${message.notification.android.clickAction},
        color: ${message.notification.android.color},
        count: ${message.notification.android.count},
        imageUrl: ${message.notification.android.imageUrl},
        link: ${message.notification.android.link},
        priority: ${message.notification.android.priority},
        smallIcon: ${message.notification.android.smallIcon},
        sound: ${message.notification.android.sound},
        ticker: ${message.notification.android.ticker},
        visibility: ${message.notification.android.visibility},
      ),
      apple: ${message.notification.apple},
      title: ${message.notification.title},
      titleLocArgs: ${message.notification.titleLocArgs},
      titleLocKey: ${message.notification.titleLocKey},
      body: ${message.notification.body},
      bodyLocArgs: ${message.notification.bodyLocArgs},
      bodyLocKey: ${message.notification.bodyLocKey},
    ),
    sentTime: ${message.sentTime},
    threadId: ${message.threadId},
    ttl: ${message.ttl},
  ),
)''';
}
