import 'package:catcher/catcher.dart';
import 'package:catcher/model/platform_type.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

class SentryReportHandler extends ReportHandler {
  @override
  Future<bool> handle(Report error) async {
    await Sentry.captureException(
      error.error,
      stackTrace: error.stackTrace,
    );
    return true;
  }

  @override
  List<PlatformType> getSupportedPlatforms() => [
        PlatformType.Android,
        PlatformType.iOS,
        PlatformType.Web,
      ];
}
