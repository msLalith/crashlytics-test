import 'package:flutter/services.dart' show MethodChannel;

class CrashChannel {
  static const String _CRASH_CHANNEL_NAME = "com.crashlytics_test_channel";

  static const MethodChannel _crashChannel = MethodChannel(_CRASH_CHANNEL_NAME);

  static Future<void> crashFromAndroid() async {
    await _crashChannel.invokeMethod('crashFromAndroid');
  }

  static Future<void> crashFromIOS() async {
    await _crashChannel.invokeMethod('crashFromIOS');
  }
}
