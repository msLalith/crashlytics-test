import 'package:catcher/catcher.dart';
import 'package:catcher/model/platform_type.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

class CrashlyticsHandler extends ReportHandler {
  @override
  Future<bool> handle(Report error) async {
    await FirebaseCrashlytics.instance.recordError(
      error.error,
      error.stackTrace,
    );
    return true;
  }

  @override
  List<PlatformType> getSupportedPlatforms() => [
        PlatformType.Android,
        PlatformType.iOS,
        PlatformType.Web,
      ];
}
