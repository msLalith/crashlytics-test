import 'package:crashlytics_test/di/locator.dart';
import 'package:crashlytics_test/services/notification_service.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';

class PopUpNotification extends StatelessWidget {
  final String title;
  final String body;
  final String payload;
  final Color backgroundColor;
  final Color textColor;
  final double backgroundOpacity;

  const PopUpNotification({
    Key key,
    @required this.title,
    @required this.body,
    this.payload,
    this.backgroundColor = Colors.black,
    this.textColor = Colors.white,
    this.backgroundOpacity = 0.8,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final statusBarHeight = mediaQuery.padding.top;
    final screenWidth = mediaQuery.size.width;
    final contentPadding = 24.0;

    return Dismissible(
      key: ValueKey('$title-$body'),
      direction: DismissDirection.up,
      onDismissed: (direction) {
        OverlaySupportEntry.of(context).dismiss(animate: false);
      },
      child: Material(
        color: backgroundColor.withOpacity(backgroundOpacity),
        child: Container(
          width: screenWidth,
          padding: EdgeInsets.only(top: statusBarHeight),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: contentPadding),
                child: Theme(
                  data: ThemeData(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                  ),
                  child: ListTile(
                    leading: CircleAvatar(
                      backgroundImage: AssetImage('images/dummy_avatar.jpg'),
                    ),
                    title: Text(
                      title,
                      style: TextStyle(color: textColor),
                    ),
                    subtitle: Text(
                      body,
                      style: TextStyle(color: textColor),
                    ),
                    onTap: () {
                      OverlaySupportEntry.of(context).dismiss();
                      getIt<NotificationService>()
                          .onSelectNotification(payload);
                    },
                  ),
                ),
              ),
              _slideHandler(
                width: screenWidth * 0.1,
                verticalSpacing: contentPadding / 2,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _slideHandler({
    @required double width,
    @required double verticalSpacing,
  }) {
    return Container(
      width: width,
      height: 6.0,
      margin: EdgeInsets.symmetric(vertical: verticalSpacing),
      decoration: BoxDecoration(
        color: textColor,
        borderRadius: BorderRadius.circular(60.0),
      ),
    );
  }
}
